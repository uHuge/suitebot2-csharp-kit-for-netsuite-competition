﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SuiteBot2.Game;

namespace SuiteBot2.Json
{
	public static class JsonUtil
	{
		public enum MessageType
		{
			Setup,
			Moves
		}

		public static MessageType DecodeMessageType(string json)
		{
			JObject parsedJson = JObject.Parse(json);

			string messageType = parsedJson["messageType"].ToString();

			return (MessageType) Enum.Parse(typeof(MessageType), messageType, true);
		}

		public static GameRound DecodeMovesMessage(string json)
		{
			return JsonConvert.DeserializeObject<GameRound>(json);
		}

		public static GameSetup DecodeSetupMessage(string json)
		{
			return JsonConvert.DeserializeObject<GameSetup>(json);
		}
	}
}
