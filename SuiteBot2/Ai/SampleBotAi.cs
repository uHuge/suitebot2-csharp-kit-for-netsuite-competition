﻿using System.Collections.Generic;
using SuiteBot2.Game;

namespace SuiteBot2.Ai
{
	public class SampleBotAi : IBotAi
	{
		public static class Move
		{
			public const string Fire = "FIRE";
			public const string GoLeft = "GO LEFT";
			public const string WarmUp = "WARM UP";
		}

		public string InitializeAndMakeMove(GameSetup gameSetup)
		{
			EnemyFired = false;
			GamePlan = gameSetup.GamePlan;
			MyId = gameSetup.AiPlayerId;
			Players = gameSetup.Players;
			Round = 1;

			SetMyStartingPosition();

			return FigureOutMove();
		}

		public string MakeMove(GameRound gameRound)
		{
			Round++;
			EnemyFired = false;

			foreach (PlayerMove playerMove in gameRound.Moves)
				SimulateMove(playerMove);

			return FigureOutMove();
		}

		private string FigureOutMove()
		{
			if (EnemyFired)
			{
				// If they have fired, we will fire too!
				return Move.Fire;
			}
			else
			{
				if (Round <= GamePlan.MaxRounds / 2)
					return Move.WarmUp;
				else
					return Move.GoLeft;
			}
		}

		private void SetMyStartingPosition()
		{
			int myIndex = Players.IndexOf(new Player(MyId, null));

			MyPosition = GamePlan.StartingPositions[myIndex];
		}

		private void SimulateMove(PlayerMove playerMove)
		{
			if (playerMove.PlayerId != MyId && Move.Fire.Equals(playerMove.Move))
				EnemyFired = true;
		}

		private bool EnemyFired { get; set; }

		private GamePlan GamePlan { get; set; }

		private int MyId { get; set; }

		private Point MyPosition { get; set; }

		private List<Player> Players { get; set; }

		private int Round { get; set; }
	}
}
