﻿namespace SuiteBot2.Server
{
	public interface ISimpleRequestHandler
	{
		string ProcessRequest(string request);
	}
}
