﻿namespace SuiteBot2.Game
{
	public class Player
	{
		public Player(int id, string name)
		{
			Id = id;
			Name = name;
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
				return true;

			if (obj == null || GetType() != obj.GetType())
				return false;

			Player player = (Player) obj;

			return Id == player.Id;
		}

		public override int GetHashCode()
		{
			return Id;
		}

		public override string ToString()
		{
			return "Player{" +
				"id=" + Id +
				", name='" + Name + '\'' +
				'}';
		}

		public int Id { get; private set; }

		public string Name { get; private set; }
	}
}
